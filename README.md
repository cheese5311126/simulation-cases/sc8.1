# Magmatic systems and induced landslides: the Etna case

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/Mainz.png?inline=false" width="100">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="80">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="40">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/Sorbonne-Univ.png?inline=false" width="90">

**Codes:** [LaMEM](https://gitlab.com/cheese5311126/codes/lamem/lamem)

**Target EuroHPC Architectures:** LUMI, MeluXina

**Type:** Hazard assessment

## Description

A significant risk related to volcanic eruptions is that the flank of a volcano can
collapse before or during an eruption, which can induce tsunamis or generate
even larger eruptions. A particularly relevant case is the southern flank of Etna,
which is currently sliding seawards with several cm’s per year. It is crucial to
create numerical models of these processes, in order to estimate whether future
changes in the magmatic system may cause the sliding to become catastrophic
(and induce a major eruption/tsunami). Simulation case developed by
collaborator from University of Mainz (UM) for ChEESE-2P project and utilises UM
developed code LaMEM (Kaus et al., 2016). LaMEM is used to deal with
geodynamic related topics of Exascale Computational Challenges (ECC): ECC11
(Landslide triggering) and ECC12 (Lithospheric stress states and induced
seismicity).
The SC will provide the first 3D models of the ongoing deformation at Etna to
determine the slide plane that can explain the ongoing deformation. In addition,
it will test the sensitivity of the deformation to the thermal and magmatic
structure, as well as to material parameters. The most successful 3D simulations
will be made available and can also be used to estimate volumes of rock mass
that may induce tsunamis. Ultimately, this should be embedded in an inversion
framework, but in our experience with performing such geodynamic inversions, it
is crucial to have good starting models (and fast forward simulations) which will
be obtained here.

<img src="SC8.1.png" width="700">

**Figure 3.12.1.** Graphical abstract of SC8.1.

## Expected results

The SC will provide the first 3D models of the ongoing deformation at Etna to determine the slide plane
that can explain the ongoing deformation. In addition, it will test the sensitivity of the deformation to the thermal and
magmatic structure, as well as to material parameters. The most successful 3D simulations will be made available and can
also be used to estimate volumes of rock mass that may induce tsunamis. Ultimately, this should be embedded in an inversion
framework, but in our experience with performing such geodynamic inversions, it is crucial to have good starting models
(and fast forward simulations) which will be obtained here.